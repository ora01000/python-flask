import cx_Oracle

from flask import Flask, session, redirect, url_for, request, render_template


application = Flask(__name__)


@application.route('/')
def login_form():
    return render_template('login_form.html')

@application.route('/user')
def showUserName():
    return render_template('user.html', name=session['userName'])

@application.route('/login', methods=['POST'])
def login():


    con = cx_Oracle.connect("scott/tiger@xe")
    cur = con.cursor()
    cur.execute("select * from tab")

    for row in cur:
        print(row)
    cur.close()
    con.close()

    if request.method == 'POST':
        session['userName'] = request.form['userName']
        return redirect(url_for('showUserName'))
    else:
        return 'login failed'


@application.route('/post/<int:postId>')
def showPost(postId):
    return postId

application.secret_key = 'idsaosdafoidsflakfasdfasdkljasldfkjaslkj'

if __name__ == '__main__':
    application.run()
